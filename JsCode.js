var mousePressed = false;
var lastX, lastY;
var ctx;

function InitThis() {
    ctx = document.getElementById('myCanvas').getContext("2d");

    $('#myCanvas').mousedown(function (e) {
	    mousePressed = true;
	    Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
	});

    $('#myCanvas').mousemove(function (e) {
	    if (mousePressed) {
		Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
	    }
	});

    $('#myCanvas').mouseup(function (e) {
	    mousePressed = false;
	});
    $('#myCanvas').mouseleave(function (e) {
	    mousePressed = false;
	});
}

function Draw(x, y, isDown) {
    if (isDown) {
        ctx.beginPath();
        ctx.strokeStyle = $('#selColor').val();
        ctx.lineWidth = $('#selWidth').val();
        ctx.lineJoin = "round";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
    }
    lastX = x; lastY = y;
}

function clearArea() {
    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function redraw(){
    context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
  
    context.strokeStyle = "#df4b26";
    context.lineJoin = "round";
    context.lineWidth = 5;
    
    for(var i=0; i < clickX.length; i++) {
	context.beginPath();
	if(clickDrag[i] && i){
	    context.moveTo(clickX[i-1], clickY[i-1]);
	}else{
	    context.moveTo(clickX[i]-1, clickY[i]);
	}
	context.lineTo(clickX[i], clickY[i]);
	context.closePath();
	context.stroke();
    }

function convertCanvastoImage(myCanvas) {
    var image = new Image();
    var canvas = document.getElementById("myCanvas");
    image.src = canvas.toDataURL("image/png");
    return image;
  }

}